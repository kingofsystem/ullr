from rest_framework import serializers

from . import models


class CitySerializer(serializers.ModelSerializer):
    """Serializer for City model"""

    class Meta:
        model = models.City
        fields = '__all__'


# noinspection PyAbstractClass
class WeatherEntryListSerializer(serializers.ListSerializer):
    """
    List serializer that validates multiple WeatherEntry entities between each
    other
    """

    def validate(self, attrs):
        """Checks that all data are unique"""

        pairs = {
            (entry['city'], entry['date']) for entry in attrs
        }
        if len(pairs) != len(attrs):
            raise serializers.ValidationError(
                code='unique',
            )
        return attrs

    def create(self, validated_data):
        """Creates objects from validated data with bulk_create"""

        return models.WeatherEntry.objects.bulk_create(
            models.WeatherEntry(**entry_data)
            for entry_data in validated_data
        )


class WeatherEntrySerializer(serializers.ModelSerializer):
    """Serializer for WeatherEntry model"""

    class Meta:
        model = models.WeatherEntry
        fields = '__all__'
        list_serializer_class = WeatherEntryListSerializer
