from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import permissions, viewsets
from rest_framework.filters import OrderingFilter
from rest_framework.response import Response

from django.db.models import Avg

from . import filters, models, serializers


class CityViewSet(viewsets.ModelViewSet):
    """ViewSet for City model"""

    queryset = models.City.objects.all()
    serializer_class = serializers.CitySerializer

    permission_classes = (
        permissions.IsAuthenticatedOrReadOnly,
    )


class WeatherEntryViewSet(viewsets.ModelViewSet):
    """ViewSet for WeatherEntry model"""

    queryset = models.WeatherEntry.objects.all()
    serializer_class = serializers.WeatherEntrySerializer

    permission_classes = (
        permissions.IsAuthenticatedOrReadOnly,
    )

    filter_backends = (
        DjangoFilterBackend,
        OrderingFilter,
    )

    filter_class = filters.WeatherFilter

    ordering_fields = (
        'date',
    )
    ordering = (
        'date',
    )

    def get_serializer(self, *args, **kwargs):
        """
        To allow bulk create we need to pass many=True to serializer options
        when we got list of objects
        """

        if self.action == 'create' and isinstance(self.request.data, list):
            kwargs['many'] = True
        return super().get_serializer(*args, **kwargs)

    def filter_queryset(self, queryset):
        """
        We don't need to filter anything with  backends when we are not
        listing objects
        """

        if self.action == 'list':
            return super().filter_queryset(queryset)
        return queryset


class AvgCityTemperatureViewSet(viewsets.GenericViewSet):
    """
    Endpoint where client able to get average temp for certain period in
    certain cities
    """

    queryset = models.WeatherEntry.objects.all()

    permission_classes = (
        permissions.IsAuthenticatedOrReadOnly,
    )

    filter_backends = (
        DjangoFilterBackend,
    )

    filter_class = filters.AvgCityTemperatureFilter

    def retrieve(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        result = queryset.aggregate(
            avg_temp=Avg('temp'),
        )

        return Response(result)
