from datetime import date, timedelta

from rest_framework import status
from rest_framework.test import APITestCase

from django.contrib.auth import get_user_model
from django.db.models import Avg
from django.urls import reverse

from . import models

User = get_user_model()


class CityEndpointTestCase(APITestCase):

    list_url = reverse('city-list')
    detail_url_name = 'city-detail'

    def setUp(self):
        self.user = User.objects.create()

        # create few cities
        self.cities = models.City.objects.bulk_create(
            models.City(
                custom_id=i,
                name=f'city {i}'
            )
            for i in range(3)
        )

    def test_can_list_cities(self):
        resp = self.client.get(self.list_url)
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(len(resp.data), 3)

    def test_can_retrieve_city(self):
        resp = self.client.get(
            reverse(self.detail_url_name, args=(self.cities[0].pk,))
        )
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(resp.data['id'], self.cities[0].pk)

    def test_can_create_city(self):
        self.client.force_authenticate(self.user)

        data = {
            'custom_id': 'kek',
            'name': 'lol',
        }

        resp = self.client.post(self.list_url, data=data)
        self.assertEqual(resp.status_code, status.HTTP_201_CREATED)
        self.assertTrue(
            models.City.objects.filter(**data).exists()
        )

    def test_can_update_city(self):
        self.client.force_authenticate(self.user)

        data = {
            'custom_id': 'kek',
            'name': 'lol',
        }

        resp = self.client.put(
           reverse(self.detail_url_name, args=(self.cities[0].pk,)), data=data
        )
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertTrue(
            models.City.objects.filter(**data).exists()
        )

    def test_cant_create_duplicated_city(self):
        self.client.force_authenticate(self.user)

        data = {
            'custom_id': self.cities[0].custom_id,
            'name': self.cities[1].name,
        }

        resp = self.client.post(self.list_url, data=data)
        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(resp.data['custom_id'][0].code, 'unique')
        self.assertEqual(resp.data['name'][0].code, 'unique')

    def test_permissions(self):

        data = {
            'custom_id': 'kek',
            'name': 'lol',
        }

        resp = self.client.post(self.list_url, data=data)
        self.assertEqual(resp.status_code, status.HTTP_401_UNAUTHORIZED)


class WeatherEndpointTestCase(APITestCase):

    list_url = reverse('weather-list')
    detail_url_name = 'weather-detail'

    def setUp(self):
        self.user = User.objects.create()
        self.city = models.City.objects.create(
            custom_id='city',
            name='City',
        )

        # create few entries for city
        entry_date = date(2018, 1, 1)
        self.entries = models.WeatherEntry.objects.bulk_create(
            models.WeatherEntry(
                city=self.city,
                date=entry_date + timedelta(days=i),
                temp=i,
            )
            for i in range(31)
        )

    def test_can_list(self):

        data = {
            'city': self.city.pk,
        }
        resp = self.client.get(self.list_url, data=data)
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(len(resp.data), len(self.entries))

    def test_default_ordering(self):
        models.WeatherEntry.objects.all().delete()

        entry_date = date(2018, 1, 1)
        self.entries = models.WeatherEntry.objects.bulk_create(
            models.WeatherEntry(
                city=self.city,
                date=entry_date + timedelta(days=i),
                temp=i,
            )
            for i in reversed(range(31))
        )

        data = {
            'city': self.city.pk,
        }
        resp = self.client.get(self.list_url, data=data)
        self.assertEqual(resp.status_code, status.HTTP_200_OK, resp.data)

        for i, entry in enumerate(resp.data):
            self.assertEqual(
                entry['date'],
                (entry_date + timedelta(days=i)).isoformat(),
                entry
            )

    def test_can_retrieve(self):

        data = {
            'city': self.city.pk,
        }
        resp = self.client.get(
            reverse(self.detail_url_name, args=(self.entries[0].pk,)),
            data=data,
        )
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(resp.data['id'], self.entries[0].pk)

    def test_can_update(self):
        # TODO Make only future entries editable
        self.client.force_authenticate(self.user)

        data = {
            'city': self.city.pk,
            'date': self.entries[0].date,
            'temp': 5,
        }
        resp = self.client.put(
            reverse(self.detail_url_name, args=(self.entries[0].pk,)),
            data=data,
        )
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertTrue(
            models.WeatherEntry.objects.filter(
                pk=self.entries[0].pk,
                temp=5,
            ).exists(),
        )

    def test_can_create(self):
        self.client.force_authenticate(self.user)

        data = {
            'city': self.city.pk,
            'date': '1999-03-13',
            'temp': 5,
        }

        resp = self.client.post(self.list_url, data=data)
        self.assertEqual(resp.status_code, status.HTTP_201_CREATED)
        self.assertTrue(
            models.WeatherEntry.objects.filter(**data).exists()
        )

    def test_can_bulk_create(self):
        self.client.force_authenticate(self.user)

        data = [
            {
                'city': self.city.pk,
                'date': '1999-03-13',
                'temp': -5,
            },
            {
                'city': self.city.pk,
                'date': '1999-03-14',
                'temp': -5,
            }
        ]

        resp = self.client.post(self.list_url, data=data, format='json')
        self.assertEqual(resp.status_code, status.HTTP_201_CREATED)

        for entry in data:
            self.assertTrue(
                models.WeatherEntry.objects.filter(**entry).exists()
            )

    def test_bulk_create_unique_validation(self):
        self.client.force_authenticate(self.user)

        data = [
            {
                'city': self.city.pk,
                'date': '1999-03-13',
                'temp': -5,
            },
            {
                'city': self.city.pk,
                'date': '1999-03-13',
                'temp': -5,
            }
        ]

        resp = self.client.post(self.list_url, data=data, format='json')
        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(resp.data['non_field_errors'][0].code, 'unique')

    def test_can_delete(self):
        self.client.force_authenticate(self.user)

        resp = self.client.delete(
            reverse(self.detail_url_name, args=(self.entries[0].pk,))
        )
        self.assertEqual(resp.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(
            models.WeatherEntry.objects.filter(pk=self.entries[0].pk).exists()
        )

    def test_date_range_filter(self):

        data = {
            'city': self.city.pk,
            'date_after': '2018-01-02',
            'date_before': '2018-01-05',
        }

        resp = self.client.get(self.list_url, data=data)
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(len(resp.data), 4)
        self.assertEqual(resp.data[0]['date'], '2018-01-02')
        self.assertEqual(resp.data[3]['date'], '2018-01-05')

    def test_days_filter(self):

        data = {
            'city': self.city.pk,
            'days': (
                '2018-01-02',
                '2018-01-05',
            ),
        }

        resp = self.client.get(self.list_url, data=data)
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(len(resp.data), 2)
        self.assertEqual(resp.data[0]['date'], '2018-01-02')
        self.assertEqual(resp.data[1]['date'], '2018-01-05')


class TestAverageTempEndpoint(APITestCase):

    url = reverse('weather-avg')

    def setUp(self):
        self.user = User.objects.create()
        self.city = models.City.objects.create(
            custom_id='city',
            name='City',
        )

        # create few entries for city
        entry_date = date(2018, 1, 1)
        self.entries = models.WeatherEntry.objects.bulk_create(
            models.WeatherEntry(
                city=self.city,
                date=entry_date + timedelta(days=i),
                temp=i,
            )
            for i in range(31)
        )

    def test_computed_correctly(self):

        expected = models.WeatherEntry.objects.aggregate(
            avg=Avg('temp'),
        )['avg']

        resp = self.client.get(self.url)
        self.assertEqual(resp.status_code, status.HTTP_200_OK, resp.data)
        self.assertEqual(resp.data['avg_temp'], expected, resp.data)
