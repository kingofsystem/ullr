from django.apps import AppConfig


class WeatherConfig(AppConfig):
    name = 'weather'

    # noinspection PyUnresolvedReferences
    def ready(self):
        from . import signals
