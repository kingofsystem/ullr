from django_filters import rest_framework as filters

from . import models


class WeatherFilter(filters.FilterSet):
    """Filter class for WeatherEntry model"""

    city = filters.ModelChoiceFilter(
        queryset=models.City.objects.all(),
        required=True,
        label='City',
        help_text='City where entries placed',
    )
    date = filters.DateFromToRangeFilter(
        label='Date range',
        help_text='Ranges where date placed',
    )
    days = filters.AllValuesMultipleFilter(
        field_name='date',
        label='Days',
        help_text='Weather for certain dates',
    )

    class Meta:
        model = models.WeatherEntry
        fields = ()


class AvgCityTemperatureFilter(filters.FilterSet):
    """Filter class for avg city temperature endpoint"""

    city = filters.ModelChoiceFilter(
        queryset=models.City.objects.all(),
        required=False,
        label='City',
        help_text='City where entries placed',
    )
    date = filters.DateFromToRangeFilter(
        label='Date range',
        help_text='Ranges where date placed',
    )
    days = filters.AllValuesMultipleFilter(
        field_name='date',
        label='Days',
        help_text='Weather for certain dates',
    )

    class Meta:
        model = models.WeatherEntry
        fields = ()
