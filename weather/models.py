from django.db import models


class City(models.Model):

    custom_id = models.CharField(
        max_length=64,
        unique=True,
        help_text='Id of city from gismeteo site',
    )
    name = models.CharField(
        max_length=256,
        unique=True,
        help_text='Name of the city',
    )


class WeatherEntry(models.Model):
    """Average temperature for a certain date"""

    city = models.ForeignKey(
        to=City,
        on_delete=models.CASCADE,
        related_name='weather',
        related_query_name='weather',
        help_text='City where weather happened',
    )
    date = models.DateField(
        help_text='Date when weather happened',
    )
    temp = models.SmallIntegerField(
        help_text='Temperature for a certain date',
    )

    class Meta:
        unique_together = (
            ('city', 'date'),
        )
