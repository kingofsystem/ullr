from rest_framework import routers

from django.urls import path

from . import views

router = routers.DefaultRouter()
router.register('cities', views.CityViewSet, basename='city')
router.register('weather', views.WeatherEntryViewSet, basename='weather')

urlpatterns = [
    path(
        route='weather/average/',
        view=views.AvgCityTemperatureViewSet.as_view({
            'get': 'retrieve',
        }),
        name='weather-avg',
    )
] + router.urls
